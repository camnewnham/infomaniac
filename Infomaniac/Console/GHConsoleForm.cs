﻿using System;
using System.Windows.Forms;

namespace Infomaniac
{
    public partial class GHConsole : Form
    {
        public GHConsole()
        {
            InitializeComponent();
        }

        private void Console_TextChanged(object sender, EventArgs e)
        {
            consoleBox.SelectionStart = consoleBox.Text.Length;
            consoleBox.ScrollToCaret();
        }

        public void ClearConsole()
        {
            consoleBox.Clear();
        }

        public void AddText(String text)
        {
            consoleBox.Text += text;
        }

        public void AddTextLine(String text)
        {
            consoleBox.Text += text + "\r\n";
        }
    }
}
