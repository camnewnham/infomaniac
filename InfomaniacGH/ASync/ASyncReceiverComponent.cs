﻿using System;

using Grasshopper.Kernel;
using Grasshopper.Kernel.Data;
using Grasshopper.Kernel.Parameters;
using Grasshopper.Kernel.Types;

namespace Infomaniac.Logging
{
    public class ASyncReceiverComponent : GH_Component
    {
        public ASyncReceiverComponent()
          : base("ASync Receiver", "AReceive",
              "Allows the asynchronious release of computed information.",
              "Infomaniac", "ASync")
        {
        }

        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddGenericParameter("Trigger", "T", "A Trigger to request the asynchronious data.", GH_ParamAccess.tree);
            pManager[0].Optional = true;
        }

        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("Data Input", "<<<", "The asynchronious data sender. Connect to an ASync Sender.", GH_ParamAccess.item);
            pManager.AddGenericParameter("Data Output", ">>>", "The asynchronious received from the sender.", GH_ParamAccess.tree);
        }

        public ASyncData myData = new ASyncData();

        protected override void SolveInstance(IGH_DataAccess DA)
        {
            DA.SetData("Data Input", myData);
            DA.SetDataTree(1, myData.data);
        }

        public class ASyncData
        {
            public GH_Structure<IGH_Goo> data = new GH_Structure<IGH_Goo>();
            public override string ToString()
            {
                return "Asynchronious Data Tree: " + data.DataCount;
            }
        }

        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                //return Chromodoris.Properties.Resources.Icon_QuickSmooth;
                return null;
            }
        }

        public override Guid ComponentGuid
        {
            get { return new Guid("{FC4C05D0-543B-4DA3-A567-1CD0423D4680}"); }
        }



    }
}