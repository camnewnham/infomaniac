﻿using System;

using Grasshopper.Kernel;
using Grasshopper.Kernel.Data;
using Grasshopper.Kernel.Parameters;
using Grasshopper.Kernel.Types;

namespace Infomaniac.Logging
{
    public class ASyncSenderComponent : GH_Component
    {
        public ASyncSenderComponent()
          : base("ASync Sender", "ASend",
              "Sends asynchronious data through one if it's inputs.",
              "Infomaniac", "ASync")
        {
        }

        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddGenericParameter("Data", "D", "The data to send asynchroniously.", GH_ParamAccess.tree);
            pManager.AddGenericParameter("Data Output", "<<<", "The asynchronious data release. Connect to an ASync Receiver.", GH_ParamAccess.item);
            
        }

        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
        }

        protected override void SolveInstance(IGH_DataAccess DA)
        {
            GH_ObjectWrapper container = null;
            if (!DA.GetData("Data Output", ref container)) return;
            var async = container.Value as ASyncReceiverComponent.ASyncData;
            if (async == null)
            {
                AddRuntimeMessage(GH_RuntimeMessageLevel.Error, "Wrong datatype as input. The data output should be connected to a receiver.");
                return;
            }

            GH_Structure<IGH_Goo> dataToSend;
            if (!DA.GetDataTree("Data", out dataToSend))
            {
                AddRuntimeMessage(GH_RuntimeMessageLevel.Warning, "No data to send.");
                return;
            }

            async.data = dataToSend.Duplicate();
        }

        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                //return Chromodoris.Properties.Resources.Icon_QuickSmooth;
                return null;
            }
        }

        public override Guid ComponentGuid
        {
            get { return new Guid("{CA83EF9F-B02C-4294-AD4F-22CACC91E84C}"); }
        }

    }
}