﻿using System;

using Grasshopper.Kernel;
using Grasshopper.Kernel.Types;
using Grasshopper.Kernel.Data;
using GH_IO.Serialization;
using System.Windows.Forms;
using Grasshopper.Kernel.Expressions;
using System.Threading;
using Infomaniac;

namespace InfomaniacGH.Console
{
    public class ConsoleComponent : GH_Component
    {
        public ConsoleComponent()
          : base("Console", "Console",
              "A pass through component for writing messages to a console during execution.",
              "Infomaniac", "Log")

        {
        }

        public override GH_Exposure Exposure
        {
            get
            {
                return GH_Exposure.tertiary;
            }
        }

        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddGenericParameter("Data", "D", "The data to pass through.", GH_ParamAccess.tree);
            pManager[0].Optional = true;
            pManager.AddTextParameter("Text", "T", "The text to log", GH_ParamAccess.tree);
        }

        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("Data", "D", "The passed through data.", GH_ParamAccess.tree);
        }

        protected override void SolveInstance(IGH_DataAccess DA)
        {
            GH_Structure<IGH_Goo> structure;
            DA.GetDataTree<IGH_Goo>("Data", out structure);

            GH_Structure<GH_String> txtStruct;
            DA.GetDataTree<GH_String>("Text", out txtStruct);

            foreach (var item in txtStruct.AllData(false))
            {
                String msg = "";
                if (includeTimeInLog) msg += String.Format("{0:T}", DateTime.Now) + ": ";

                msg += item;
                myConsole.AddTextLine(msg);
            }

            DA.SetDataTree(0, structure);
        }

        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                //return Chromodoris.Properties.Resources.Icon_QuickSmooth;
                return null;
            }
        }

        public override Guid ComponentGuid
        {
            get { return new Guid("{9C8B1985-8194-43DB-992E-24C201287215}"); }
        }

        #region console

        public GHConsole myConsole;

        private GH_Document doc;

        public override void AddedToDocument(GH_Document document)
        {
            this.doc = document;
            doc.ContextChanged += DocumentContextChanged;
            if (!document.ConstantServer.ContainsKey("InfomaniacConsole"))
            {
                var console = new GHConsole();
                document.ConstantServer.Add("InfomaniacConsole", new ConsoleConstant(console));
                this.myConsole = console;
                ShowFormNewThread();
            }
            else
            {
                this.myConsole = ((ConsoleConstant)document.ConstantServer["InfomaniacConsole"]).theConsole;
            }
            base.AddedToDocument(document);
        }

        private void DocumentContextChanged(object sender, GH_DocContextEventArgs e)
        {
            if (e.Context == GH_DocumentContext.Close)
            {
                if (myConsole != null)
                {
                    myConsole.Close();
                }
            }
        }

        public class ConsoleConstant : GH_Variant
        {
            public GHConsole theConsole;
            public ConsoleConstant(GHConsole newConsole)
            {
                theConsole = newConsole;
            }
        }

        private void ShowFormNewThread()
        {
            Thread ConsoleThread = new Thread(new ThreadStart(ThreadProc));
            ConsoleThread.Start();
        }

        private void ThreadProc()
        {
            myConsole.ShowDialog();
        }

        #endregion

        #region menus

        private bool includeTimeInLog = true;

        public override void AppendAdditionalMenuItems(ToolStripDropDown menu)
        {
            Menu_AppendItem(menu, "Include Time", IncludeTimeToggled, true, includeTimeInLog);
            Menu_AppendSeparator(menu);
            menu.Items.Add(new ToolStripButton("Show", null, MenuButtonClicked));
            menu.Items.Add(new ToolStripButton("Hide", null, MenuButtonClicked));
            menu.Items.Add(new ToolStripButton("Clear", null, MenuButtonClicked));
        }

        private void IncludeTimeToggled(object sender, EventArgs args)
        {
            includeTimeInLog = ((ToolStripMenuItem)sender).Checked;
        }

        private void MenuButtonClicked(object sender, EventArgs args)
        {
            switch (((ToolStripButton) sender).Text) {
                case "Show":
                    if (!myConsole.Visible) ShowFormNewThread();
                    else
                    {
                        myConsole.BringToFront();
                    }
                    break;
                case "Hide":
                    if (myConsole.Visible) myConsole.Hide();
                    break;

                case "Clear":
                    myConsole.ClearConsole();
                    break;
                default:
                    break;
            }
        }

        public override bool Write(GH_IWriter writer)
        {
            writer.SetBoolean("includeTimeInLog", includeTimeInLog);
            return base.Write(writer);
        }

        public override bool Read(GH_IReader reader)
        {
            reader.TryGetBoolean("includeTimeInLog", ref includeTimeInLog);
            return base.Read(reader);
        }

        #endregion
    }
}