﻿using System;

using Grasshopper.Kernel;
using Rhino.Geometry;
using Grasshopper.Kernel.Types;

namespace InfomaniacGH
{
        public class ConvertTypeComponent : GH_Component
        {
            public ConvertTypeComponent()
              : base("Convert Type", "Convert",
                  "Extracts the underlying object from a grasshopper wrapped item.",
                  "Infomaniac", "Convert")
            {
            }

            public override GH_Exposure Exposure
            {
                get
                {
                    return GH_Exposure.quinary;
                }
            }

            protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
            {
                pManager.AddGenericParameter("Object to Convert", "O", "The object to convert", GH_ParamAccess.item);
            }

            protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
            {
                pManager.AddGenericParameter("Converted Object", "O", "The underlying object", GH_ParamAccess.item);
            }

            protected override void SolveInstance(IGH_DataAccess DA)
            {
                object obj = null;
                if (!DA.GetData("Object to Convert", ref obj)) return;
                // ref http://stackoverflow.com/questions/14129421/get-property-of-generic-class
                Type t = obj.GetType();
                System.Reflection.PropertyInfo prop = t.GetProperty("Value");
                object data = prop.GetValue(obj, null);
                DA.SetData("Converted Object", data);
            }

            protected override System.Drawing.Bitmap Icon
            {
                get
                {
                    return null;
                }
            }

            public override Guid ComponentGuid
            {
                get { return new Guid("{E789D2A0-AA5B-41B3-9894-E1ED57977E82}"); }
            }
        }
    }
