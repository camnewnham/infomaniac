﻿using System;

using Grasshopper.Kernel;
using Grasshopper.Kernel.Data;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using Grasshopper.Kernel.Types;

namespace InfomaniacGH
{
    public class ListFilesComponent : GH_Component
    {
        public ListFilesComponent()
          : base("List Files", "ListFiles",
              "Lists the files in a given folder.",
              "Infomaniac", "Files")
        {
        }

        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddTextParameter("Search Path", "P", "The path to search for files or folders", GH_ParamAccess.item);
            pManager.AddTextParameter("Search Mask", "M", "Only returns results that contain the specified string", GH_ParamAccess.item, "*");
            pManager[1].Optional = true;
            pManager.AddBooleanParameter("Include Subfolders", "F", "Optionally search subfolders as well.", GH_ParamAccess.item, false);
            pManager[2].Optional = true;
        }

        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddTextParameter("Files", "F", "The files in the search folder.", GH_ParamAccess.list);
        }

        protected override void SolveInstance(IGH_DataAccess DA)
        {
            String path = null;
            String mask = "*";
            bool includeSubfolders = false;
            if (!DA.GetData("Search Path", ref path)) return;
            DA.GetData("Search Mask", ref mask);
            DA.GetData("Include Subfolders", ref includeSubfolders);

            path = Path.GetFullPath(path);

            List<string> files = new List<string>();
            DA.SetDataList("Files", Directory.GetFiles(path, mask, includeSubfolders ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly).Select(x => new GH_String(x)));
        }

        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                //return Resources.IconForThisComponent;
                return null;
            }
        }

        public override Guid ComponentGuid
        {
            get { return new Guid("{8238A195-9706-4F00-A099-F77FDE97D87E}"); }
        }
    }
}
