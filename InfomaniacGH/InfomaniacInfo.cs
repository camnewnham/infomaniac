﻿using System;
using System.Drawing;
using Grasshopper.Kernel;

namespace Infomaniac
{
    public class InfomaniacInfo : GH_AssemblyInfo
    {
        public override string Name
        {
            get
            {
                return "Infomaniac";
            }
        }
        public override Bitmap Icon
        {
            get
            {
                //Return a 24x24 pixel bitmap to represent this GHA library.
                return null;
            }
        }
        public override string Description
        {
            get
            {
                //Return a short string describing the purpose of this GHA library.
                return "A utility plugin for dealing with additional grasshopper information, ie. file formats, asynchronious operations, logging, files etc.";
            }
        }
        public override Guid Id
        {
            get
            {
                return new Guid("e46a879f-4c63-46d0-b043-0047761e1d77");
            }
        }

        public override string AuthorName
        {
            get
            {
                //Return a string identifying you or your company.
                return "Cam Newnham";
            }
        }
        public override string AuthorContact
        {
            get
            {
                //Return a string representing your preferred contact details.
                return "RMIT University";
            }
        }
    }
}
