﻿using System;

using Grasshopper.Kernel;
using Grasshopper.Kernel.Types;
using Grasshopper.Kernel.Data;
using GH_IO.Serialization;
using System.Windows.Forms;

namespace Infomaniac.Logging
{
    public class TimeComponent : GH_Component
    {
        public TimeComponent()
          : base("Execution Time", "Exec",
              "A pass through component for recording the time in which a section was computed. Optionally plays a sound when it is executed.",
              "Infomaniac", "Log")
        {
            Sound = PlaySound.NONE;
        }

        public override GH_Exposure Exposure
        {
            get
            {
                return GH_Exposure.quarternary;
            }
        }

        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddGenericParameter("Data", "D", "The data to pass through.", GH_ParamAccess.tree);
            pManager[0].Optional = true;
        }

        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("Data", "D", "The data.", GH_ParamAccess.tree);
            pManager.AddTimeParameter("Time", "T", "The date and time of execution", GH_ParamAccess.item);
        }

        protected override void SolveInstance(IGH_DataAccess DA)
        {
            GH_Structure<IGH_Goo> structure;
            DA.GetDataTree<IGH_Goo>("Data", out structure);

            switch ((PlaySound) Sound)
            {
                case PlaySound.NONE:
                    break;
                case PlaySound.ASTERICK:
                    System.Media.SystemSounds.Asterisk.Play();
                    break;
                case PlaySound.BEEP:
                    System.Media.SystemSounds.Beep.Play();
                    break;
                case PlaySound.EXCLAMATION:
                    System.Media.SystemSounds.Exclamation.Play();
                    break;
                case PlaySound.HAND:
                    System.Media.SystemSounds.Hand.Play();
                    break;
                case PlaySound.QUESTION:
                    System.Media.SystemSounds.Question.Play();
                    break;
                default:
                    break;
            }

            DA.SetDataTree(0, structure);
            DA.SetData("Time", new GH_Time(DateTime.Now));
        }

        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                //return Chromodoris.Properties.Resources.Icon_QuickSmooth;
                return null;
            }
        }

        public override Guid ComponentGuid
        {
            get { return new Guid("{6AEA67EF-450F-4426-A1BD-4CC6AB1EFBE2}"); }
        }

        #region soundOptions

        private PlaySound sound = PlaySound.NONE;

        public PlaySound Sound
        {
            get { return sound; }
            set
            {
                sound = value;
                switch (sound)
                {
                    case PlaySound.NONE:
                        Message = null;
                        break;
                    case PlaySound.ASTERICK:
                        Message = "Asterick";
                        break;
                    case PlaySound.BEEP:
                        Message = "Beep";
                        break;
                    case PlaySound.EXCLAMATION:
                        Message = "Exclamation";
                        break;
                    case PlaySound.HAND:
                        Message = "Hand";
                        break;
                    case PlaySound.QUESTION:
                        Message = "Question";
                        break;
                    default:
                        break;
                }
            }
        }


        public enum PlaySound
        {
            NONE, ASTERICK, BEEP, EXCLAMATION, HAND, QUESTION
        }

        public override void AppendAdditionalMenuItems(ToolStripDropDown menu)
        {
            var header = new ToolStripMenuItem("Sound Settings");
            header.Enabled = false;
            header.ToolTipText = "Optionally play a sound when data passes through this component.";
            menu.Items.Add(header);

            menu.Items.Add(new ToolStripButton("None", null, Scroller_ValueChanged));
            menu.Items.Add(new ToolStripButton("Asterick", null, Scroller_ValueChanged));
            menu.Items.Add(new ToolStripButton("Beep", null, Scroller_ValueChanged));
            menu.Items.Add(new ToolStripButton("Exclamation", null, Scroller_ValueChanged));
            menu.Items.Add(new ToolStripButton("Hand", null, Scroller_ValueChanged));
            menu.Items.Add(new ToolStripButton("Question", null, Scroller_ValueChanged));
        }

        private void Scroller_ValueChanged(object sender, EventArgs args)
        {
            Sound = (PlaySound) Enum.Parse(typeof(PlaySound), ((ToolStripButton) sender).Text.ToUpper()); ;
        }

        public override bool Write(GH_IWriter writer)
        {
            writer.SetInt32("sound", (int) Sound);
            return base.Write(writer);
        }

        public override bool Read(GH_IReader reader)
        {
            int soundInt = 0;
            reader.TryGetInt32("sound", ref soundInt);
            Sound = (PlaySound)soundInt;
            return base.Read(reader);
        }
        #endregion
    }
}