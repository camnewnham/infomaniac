﻿using System;

using Grasshopper.Kernel;
using Grasshopper.Kernel.Types;

namespace Infomaniac.Logging
{
    public class TimeSpanComponent : GH_Component
    {
        public TimeSpanComponent()
          : base("TimeSpan", "Span",
              "Returns the number of seconds between two times.",
              "Infomaniac", "Log")
        {
        }

        public override GH_Exposure Exposure
        {
            get
            {
                return GH_Exposure.quarternary;
            }
        }

        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddTimeParameter("Time 1", "A", "The earlier time.", GH_ParamAccess.item, epoch);
            pManager[0].Optional = true;
            pManager.AddTimeParameter("Time 2", "B", "The later time.", GH_ParamAccess.item);
        }

        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddNumberParameter("Seconds", "S", "The timespan in seconds.", GH_ParamAccess.item);
        }

        public GH_Document doc;
        public static readonly DateTime epoch = new DateTime(1970, 1, 1);

        protected override void SolveInstance(IGH_DataAccess DA)
        {
            DateTime time1 = new DateTime();
            DateTime time2 = new DateTime();
            DA.GetData("Time 1", ref time1);
            if (!DA.GetData("Time 2", ref time2)) return;

            DA.SetData("Seconds", new GH_Number((time2 - time1).TotalSeconds));
        }

        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                //return Chromodoris.Properties.Resources.Icon_QuickSmooth;
                return null;
            }
        }

        public override Guid ComponentGuid
        {
            get { return new Guid("{E0AEE7D9-A68A-4C47-8312-F9C9278A428F}"); }
        }
    }
}