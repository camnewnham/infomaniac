﻿using System;

using Grasshopper.Kernel;
using Grasshopper.Kernel.Data;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using Grasshopper.Kernel.Types;
using Infomaniac.Output;
using Rhino.Geometry;
using System.Drawing;

namespace InfomaniacGH
{
    public class SVGFormatComponent : GH_Component
    {
        public SVGFormatComponent()
          : base("SVG Format", "SVG",
              "Creates an the text required in a .svg file.",
              "Infomaniac", "Files")
        {
        }

        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddGenericParameter("SVG Properties", "P", "The svg properties.", GH_ParamAccess.list);
        }

        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddTextParameter("SVG", "S", "The svg file.", GH_ParamAccess.item);
        }

        protected override void SolveInstance(IGH_DataAccess DA)
        {
            List<object> svgObjs = new List<object>();
            if (!DA.GetDataList("SVG Properties", svgObjs)) return;
            List<SVGLineTypeProperties> prop = new List<SVGLineTypeProperties>();
            foreach (var obj in svgObjs)
            {
                var ow = (GH_ObjectWrapper) obj;
                SVGLineTypeProperties p = ow.Value as SVGLineTypeProperties;
                if (p == null)
                { 
                    AddRuntimeMessage(GH_RuntimeMessageLevel.Error, "Input should be SVG Properties.");
                    return;
                }
                else
                {
                    prop.Add(p);
                }
            }



            DA.SetData("SVG", SVGFormatter.GetSVG(prop));
        }

        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                //return Resources.IconForThisComponent;
                return null;
            }
        }

        public override Guid ComponentGuid
        {
            get { return new Guid("{3B7265A4-FEBE-4B43-B942-FCF9D1961497}"); }
        }
    }
}
