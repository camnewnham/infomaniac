﻿using System;

using Grasshopper.Kernel;
using Grasshopper.Kernel.Data;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using Grasshopper.Kernel.Types;
using Infomaniac.Output;
using Rhino.Geometry;
using System.Drawing;

namespace InfomaniacGH
{
    public class SVGParametersComponent : GH_Component
    {
        public SVGParametersComponent()
          : base("SVG LineType Properties", "SVGParam",
              "Creates an object for SVG writing",
              "Infomaniac", "Files")
        {
        }

        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddCurveParameter("Curve", "C", "The curve or region.", GH_ParamAccess.item);
            pManager[pManager.AddColourParameter("Stroke", "S", "The stroke color.", GH_ParamAccess.item)].Optional = true;
            pManager[pManager.AddNumberParameter("Weight", "W", "The stroke weight.", GH_ParamAccess.item)].Optional = true;
            pManager[pManager.AddColourParameter("Fill", "F", "The fill color.", GH_ParamAccess.item)].Optional = true;
            pManager[pManager.AddTextParameter("ID", "I", "Optional ID tag for JS.", GH_ParamAccess.item)].Optional = true;
            pManager[pManager.AddTextParameter("Style", "S", "Additional css stylings, ie. \"opacity:0.25;\".", GH_ParamAccess.item)].Optional = true;
            pManager[pManager.AddIntegerParameter("Draw Order", "D", "The draw order (0=first, 1=second...n=last).", GH_ParamAccess.item)].Optional = true;
        }

        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("SVG Properties", "P", "The svg properties.", GH_ParamAccess.item);
        }

        protected override void SolveInstance(IGH_DataAccess DA)
        {
            SVGLineTypeProperties prop = new SVGLineTypeProperties();
            Curve curve = null;
            Color stroke = Color.Black;
            double weight = 1;
            Color fill = Color.White;
            String id = null;
            String styles = null;
            int drawOrder = 0;

            if (!DA.GetData("Curve", ref curve)) return;
            prop.curve = curve;
            if (DA.GetData("Stroke", ref stroke)) prop.stroke = stroke;
            if (DA.GetData("Weight", ref weight)) prop.weight = weight;
            if (DA.GetData("Fill", ref fill)) prop.fill = fill;
            if (DA.GetData("ID", ref id)) prop.id = id;
            if (DA.GetData("Style", ref styles)) prop.style = styles;
            if (DA.GetData("Draw Order", ref drawOrder)) prop.drawOrder = drawOrder;

            DA.SetData("SVG Properties", new GH_ObjectWrapper(prop));
        }

        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                //return Resources.IconForThisComponent;
                return null;
            }
        }

        public override Guid ComponentGuid
        {
            get { return new Guid("{7707E56A-2479-4F7C-802E-3C029385192B}"); }
        }
    }
}
