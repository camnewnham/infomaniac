﻿using Rhino.Geometry;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace Infomaniac.Output
{
    static class SVGFormatter
    {
        public static String GetSVG(IEnumerable<SVGLineTypeProperties> svgProps)
        {
            List<String> svgLines = new List<String>();

            int width;
            int height;
            GetMax(svgProps, out width, out height);

            svgLines.Add(String.Format("<svg width=\"{0}\" height=\"{1}\" xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\">", width, height));
            Dictionary<String, String> classDict = SVGAddClasses(svgProps, ref svgLines);
            SVGAddPolylines(svgProps, ref svgLines, ref classDict);
            svgLines.Add("</svg>");
            return String.Join("\r\n", svgLines);
        }


        public static void GetMax(IEnumerable<SVGLineTypeProperties> svgProps, out int width, out int height)
        {
            List<Curve> allCrvs = svgProps.Select(x => x.curve).ToList();
            Box b = new Box(allCrvs[0].GetBoundingBox(true));
            double maxX = b.X.Max;
            double maxY = b.Y.Max;
            for (int i = 0; i < allCrvs.Count; i++)
            {
                BoundingBox bb2 = allCrvs[i].GetBoundingBox(true);
                if (bb2.Max.X > maxX) maxX = bb2.Max.X;
                if (bb2.Max.Y > maxY) maxY = bb2.Max.Y;
            }

            width = (int)Math.Ceiling(maxX);
            height = (int)Math.Ceiling(maxY);

        }

        public static Dictionary<String, String> SVGAddClasses(IEnumerable<SVGLineTypeProperties> svgProps, ref List<String> svgLines)
        {

            List<String> cssClasses = new List<String>();
            HashSet<String> classHash = new HashSet<String>();
            Dictionary<String, String> classIndexLookup = new Dictionary<String, String>();
            int index = 0;
            foreach (var p in svgProps)
            {
                String nm = p.ClassName;
                if (classHash.Add(nm))
                {
                    cssClasses.Add(p.GetCSSForClass(index));
                    classIndexLookup.Add(p.ClassName, "dwg"+index.ToString());
                    index++;
                }
            }

            svgLines.Add(@"  <defs>");
            svgLines.Add("    <style type=\"text/css\">");
            svgLines.Add(@"     <![CDATA[");

            foreach (String c in cssClasses)
                svgLines.Add("       " + c);

            svgLines.Add(@"     ]]>");
            svgLines.Add(@"    </style>");
            svgLines.Add(@"  </defs>");
            // .lt1{fill:none;stroke:blue;stroke-width:1;stroke-linecap:round}
            // <rect x="200" y="100" width="600" height="300" style = "fill: red; stroke: blue; stroke-width: 3"/>
            return classIndexLookup;
        }

        public static void SVGAddPolylines(IEnumerable<SVGLineTypeProperties> svgProps, ref List<string> svgLines, ref Dictionary<String, String> classDict)
        {
            svgLines.Add("  <g id=\"drawing\">");

            foreach (var prop in svgProps.OrderBy(x => x.drawOrder))
            {
                svgLines.Add("    "+prop.GetDrawInfo(classDict[prop.ClassName]));
            }
            svgLines.Add("  </g>");
            //<polyline xmlns="http://www.w3.org/2000/svg" points="50,150 50,200 200,200 200,100" stroke="red" stroke-width="4" fill="none"/>
        }
    }

    class SVGLineTypeProperties
    {
        public Curve curve = null;
        public Color? stroke = null;
        public double? weight = null;
        public Color? fill = null;
        public int? drawOrder = null;
        public string id = null;
        public string style = null;

        public String ClassName
        {
            get
            {
                String str = "";
                if (weight.HasValue)
                {
                    str += "w" + weight.GetValueOrDefault().ToString();
                }
                if (stroke.HasValue)
                {
                    var col = stroke.GetValueOrDefault();
                    str += "sr" + col.R + "sg" + col.G + "sb" + col.B + "sa" + col.A;
                }
                if (fill.HasValue)
                {
                    var col = fill.GetValueOrDefault();
                    str += "fr" + col.R + "fg" + col.G + "fb" + col.B + "fa" + col.A;
                }
                return str;
            }
        }

        public String GetCSSForClass(int indexName)
        {
            String strokeLineCap = "round";
            String strokeLineJoin = "round";
            String fil = "";
            String strok = "";
            String classProp = "";

            if (style != null)
            {
                classProp += style;
            }

            if (fill.HasValue)
            {
                Color feel = fill.GetValueOrDefault();
                fil = String.Format("rgba({0},{1},{2},{3})", feel.R, feel.G, feel.B, (feel.A / 255d).ToString("#.##"));
                classProp += "fill:" + fil + ";";
            } else
            {
                classProp += "fill:none;";
            }

            if (stroke.HasValue)
            {
                Color stroek = stroke.GetValueOrDefault();
                strok = String.Format("rgba({0},{1},{2},{3})", stroek.R, stroek.G, stroek.B, (stroek.A / 255d).ToString("#.##"));
                classProp += "stroke:" + strok + ";";
            }
            else
            {
                classProp += "stroke:none;";
            }


            if (weight.HasValue)
            {
                classProp += "stroke-width:" + weight.GetValueOrDefault() + ";";
                classProp += "stroke-linecap:" + strokeLineCap + ";";
                classProp += "stroke-linejoin:" + strokeLineJoin + ";";
            }

            return String.Format(".{0}{{{1}}}", (indexName >= 0) ? "dwg"+indexName.ToString() : ClassName, classProp);
        }

        public String GetDrawInfo(String name)
        {
            if (curve.IsCircle())
            {
                Circle ci;
                if (curve.TryGetCircle(out ci))
                {
                    return FormattedCircle(ci, name);
                }
            }

            if (curve.IsPolyline())
            {
                Polyline pl;
                if (curve.TryGetPolyline(out pl))
                {
                    return FormattedPolylinePath(pl, name);
                }
            }
            return FormattedBezierPath(curve, name);
        }

        public String FormattedPolyline(Polyline pl, String className)
        {
            String myLine = String.Format("<polyline class=\"{0}\" ", className);
            if (id != null)
                myLine += String.Format("id=\"{0}\" ", id);
            myLine += "points=\"";
            for (int p = 0; p < pl.Count; p++)
            {
                if (p > 0) myLine += " ";
                myLine += pl[p].X.ToString("#.##") + "," + pl[p].Y.ToString("#.##");
            }
            myLine += "\"/>";
            return myLine;
        }

        public String FormattedPolylinePath(Polyline pl, String className)
        {
            String myLine = String.Format("<path class=\"{0}\" ", className);
            if (id != null)
                myLine += String.Format("id=\"{0}\" ", id);
            myLine += "d=\"M";
            int plCount = pl.IsClosed ? pl.Count - 1 : pl.Count;
            for (int p = 0; p < plCount; p++)
            {
                if (p > 0) myLine += " ";
                myLine += pl[p].X.ToString("#.##") + " " + pl[p].Y.ToString("#.##");
            }
            if (pl.IsClosed) myLine += "z";
            myLine += "\"/>";
            return myLine;
        }

        public String FormattedBezierPath(Curve c, String className)
        {
            String myLine = String.Format("<path class=\"{0}\" ", className);
            if (id != null)
                myLine += String.Format("id=\"{0}\" ", id);
            var beziers = BezierCurve.CreateCubicBeziers(c, 5, 5);

            var b = beziers[0];
            var sp = b.GetControlVertex2d(0);
            myLine += "d=\"M" + sp.X.ToString("#.##") + " " + sp.Y.ToString("#.##");

            for (int i=0; i<beziers.Length; i++)
            {
                b = beziers[i];
                var p0 = b.GetControlVertex2d(0);
                var p1 = b.GetControlVertex2d(1);
                var p2 = b.GetControlVertex2d(2);
                var p3 = b.GetControlVertex2d(3);

                myLine += " C " + p1.X.ToString("#.##") + " " + p1.Y.ToString("#.##");
                myLine += ", " + p2.X.ToString("#.##") + " " + p2.Y.ToString("#.##");
                myLine += ", " + p3.X.ToString("#.##") + " " + p3.Y.ToString("#.##");
            }
            myLine += "\"/>";
            return myLine;
        }

        public String FormattedCircle(Circle ci, String className)
        {
            String myLine = String.Format("<circle class=\"{0}\" ", className);
            if (id != null)
                myLine += String.Format("id=\"{0}\" ", id);
            myLine += String.Format("cx=\"{0}\" cy=\"{1}\" r=\"{2}\"", ci.Center.X.ToString("#.##"), ci.Center.Y.ToString("#.##"), ci.Radius.ToString("#.##"));
            myLine += "/>";
            return myLine;
        }


        public override String ToString()
        {
            return "SVG Properties";
        }
    }
}
